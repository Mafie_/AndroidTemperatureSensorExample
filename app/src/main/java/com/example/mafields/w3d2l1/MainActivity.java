package com.example.mafields.w3d2l1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.BatteryManager;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sm;
    private Sensor temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sm = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        //temp = sm.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        temp = sm.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if (temp == null) {
            TextView tv = (TextView)findViewById(R.id.textView2);
            tv.setText("Your device does not have ambient temperature sensor");
            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            BatteryTempHack bth = new BatteryTempHack();
            registerReceiver(bth, ifilter);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tv = (TextView) findViewById(R.id.textView2);
        if (temp != null) {
            float temperature = event.values[0];
            tv.setText("" + temperature);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (temp != null){
            sm.registerListener(this,temp,SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (temp != null) {
            sm.unregisterListener(this);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private class BatteryTempHack extends BroadcastReceiver {
        public BatteryTempHack() {
            super();
            TextView tv = (TextView)findViewById(R.id.textView4);
            tv.setText("Battery temperature (Not accurate if Charging or in hand)");
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            TextView tv = (TextView)findViewById(R.id.textView5);
            int temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
            float tempShow = ((float) temp) /10;
            tv.setText("" + tempShow + "°C");
        }
    }
}
